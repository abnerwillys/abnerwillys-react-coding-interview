import { useRouter } from 'next/router';
import React, { FormEventHandler, useCallback, useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';

const PersonDetail = () => {
  const router = useRouter();
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  useEffect(() => {
    load();
  }, []);

  const [name, setName] = useState('')
  const [gender, setGender] = useState('')
  const [phone, setPhone] = useState('')
  const [birthday, setBirthday] = useState('')
  const [isEditing, setIsEditing] = useState(false)

  const handleEditPersonData = useCallback(() => {
    setIsEditing(true)
    setName(data?.name)
    setGender(data?.gender)
    setPhone(data?.phone)
    setBirthday(data?.birthday)
  }, [])

  const handleSubmit = useCallback(async (event: any
    ) => {
    event.preventDefault()

    console.log(name)
    console.log(gender)
    console.log(phone)
    console.log(birthday)

    await save({
      name,
      gender,
      phone,
      birthday
    })

  }, [name, gender, phone, birthday, save])


  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={handleEditPersonData}>
            Edit
          </Button>,
        ]}
      >
        {data && (
           <form onSubmit={handleSubmit}>
              <Descriptions size="small" column={1}>
                <Descriptions.Item label="Name">
                  {isEditing ? (
                    <input type="text" value={name} onChange={(e) => setName(e.target.value)}/>
                  ) : data.name}  
                </Descriptions.Item>
                <Descriptions.Item label="Gender">
                  {isEditing ? (
                    <input type="text" value={gender} onChange={(e) => setGender(e.target.value)}/>  
                  ) : data.gender}
                </Descriptions.Item>
                <Descriptions.Item label="Phone">
                  {isEditing ? (
                    <input type="text" value={phone} onChange={(e) => setPhone(e.target.value)}/>  
                  ) : data.phone}
                </Descriptions.Item>
                <Descriptions.Item label="Birthday">
                  {isEditing ? (
                    <input type="text" value={birthday} onChange={(e) => setBirthday(e.target.value)}/>  
                  ) : data.birthday}
                </Descriptions.Item>
              </Descriptions>

              <button type="submit">Send</button>
           </form>
        )}
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
